package com.hms.http;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.wowza.util.IOPerformanceCounter;
import com.wowza.wms.application.IApplication;
import com.wowza.wms.application.IApplicationInstance;
import com.wowza.wms.client.ConnectionCounter;
import com.wowza.wms.http.HTTPProvider2Base;
import com.wowza.wms.http.HTTPServerVersion;
import com.wowza.wms.http.IHTTPRequest;
import com.wowza.wms.http.IHTTPResponse;
import com.wowza.wms.logging.WMSLogger;
import com.wowza.wms.logging.WMSLoggerFactory;
import com.wowza.wms.server.IServer;
import com.wowza.wms.server.Server;
import com.wowza.wms.vhost.IVHost;
import com.wowza.wms.vhost.VHostSingleton;

import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;

public class HttpBandwidthStats extends HTTPProvider2Base {
    
    static WMSLogger logger = WMSLoggerFactory.getLogger(HttpBandwidthStats.class);

    public void onHTTPRequest(IVHost inVhost, IHTTPRequest req, IHTTPResponse resp) {
        final List<String> vhostNames = VHostSingleton.getVHostNames();

        final IServer server = Server.getInstance();
        final ConnectionCounter counter = server.getConnectionCounter();
        final IOPerformanceCounter ioPerformance = server.getIoPerformanceCounter();

        final JsonFactory jsonFactory = new JsonFactory();

        final StringWriter out = new StringWriter();
        final StringBuilder json = new StringBuilder();

        JsonGenerator generator = null;
        try {
            generator = jsonFactory.createGenerator(out);
            generator.useDefaultPrettyPrinter();
            generator.writeStartObject();
            outputConnectionInfo(generator, server.getConnectionCounter());
            outputIOPerformanceInfo(generator, server.getIoPerformanceCounter());
            generator.writeArrayFieldStart("VHost");
            Iterator<String> iterator = vhostNames.iterator();
            while (iterator.hasNext()) {
                String vhostName = iterator.next();
                IVHost vhost = VHostSingleton.getInstance(vhostName);
                if (vhost == null) {
                    continue;
                }
                generator.writeStartObject();
                generator.writeStringField("Name", URLEncoder.encode(vhostName, "UTF-8"));
                generator.writeNumberField("TimeRunning", vhost.getTimeRunningSeconds());
                generator.writeNumberField("ConnectionsLimit", vhost.getConnectionLimit());
                outputConnectionInfo(generator, vhost.getConnectionCounter());
                outputIOPerformanceInfo(generator, vhost.getIoPerformanceCounter());
                List<String> appNames = vhost.getApplicationNames();
                generator.writeArrayFieldStart("Application");
                for (String applicationName : appNames) {
                    IApplication application = vhost.getApplication(applicationName);
                    if (application == null) {
                        continue;
                    }
                    generator.writeStartObject();
                    generator.writeStringField("Name", URLEncoder.encode(applicationName, "UTF-8"));
                    generator.writeStringField("Status", "loaded");
                    generator.writeNumberField("TimeRunning", application.getTimeRunningSeconds());
                    outputConnectionInfo(generator, application.getConnectionCounter());
                    outputIOPerformanceInfo(generator, application.getIoPerformanceCounter());
                    List<String> appInstanceNames = application.getAppInstanceNames();
                    generator.writeArrayFieldStart("ApplicationInstance");
                    for (String applicationInstanceName : appInstanceNames) {
                        IApplicationInstance instance = application.getAppInstance(applicationInstanceName);
                        if (instance == null) {
                            continue;
                        }
                        generator.writeStartObject();
                        generator.writeStringField("Name", URLEncoder.encode(applicationInstanceName, "UTF-8"));
                        generator.writeStringField("Status", "loaded");
                        generator.writeNumberField("TimeRunning", instance.getTimeRunningSeconds());
                        outputConnectionInfo(generator, instance.getConnectionCounter());
                        outputIOPerformanceInfo(generator, instance.getIOPerformanceCounter());
                        generator.writeEndObject();
                    }
                    generator.writeEndArray();
                    generator.writeEndObject();
                }
                generator.writeEndArray();
            }
            generator.writeEndObject();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (generator != null) {
                try {
                    generator.close();
                } catch (IOException e) {
                    logger.error("Probably closing json generator", e);
                }
            }
        }
        try {
            resp.setHeader("Content-Type", "application/json");
            OutputStream outputStream = resp.getOutputStream();
            byte[] outBytes = out.toString().getBytes();
            outputStream.write(outBytes);
        } catch (Exception e) {
            WMSLoggerFactory.getLogger(HTTPServerVersion.class).error("HTTPServerInfoXML.onHTTPRequest: " + e.toString());
            e.printStackTrace();
        }
    }

    private void outputConnectionInfo(JsonGenerator generator, ConnectionCounter counter) throws IOException {
        generator.writeNumberField("ConnectionsCurrent", counter.getCurrent());
        generator.writeNumberField("ConnectionsTotal", counter.getTotal());
        generator.writeNumberField("ConnectionsTotalAccepted", counter.getTotalAccepted());
        generator.writeNumberField("ConnectionsTotalRejected", counter.getTotalRejected());
    }

    private void outputIOPerformanceInfo(JsonGenerator generator, IOPerformanceCounter ioPerformance) throws IOException {
        generator.writeNumberField("MessagesInBytesRate", ioPerformance.getMessagesInBytesRate());
        generator.writeNumberField("MessagesOutBytesRate", ioPerformance.getMessagesOutBytesRate());
    }
}
